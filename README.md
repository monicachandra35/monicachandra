# instructions
1) Download Source Tree
2) Choose BitBucket Cloud
3) Enter Login Details
- https://monicachandra35@bitbucket.org/monicachandra35/monicachandra.git (base url)
- monicachandra35 (your username)
- M22xxxxx (pass)
4) Click on Branch Menu - Create New Branch (your name)
5) Double Click on 'Master' Branch (make it active branch) 
- (master is local branch to merge with our changes, it's clonned from production folder)
6) Click Pull (master branch) to download all from server
- Pull Prompt: 
- Select master on 'Remote branch to pull' 
- OK
7) Double Click on '(your branchName)' Branch (make it active branch)
8) Right Click on 'Master' Branch 
- Select 'Merge master into (your branchName)' 
- Confirm Merge check list 'commit merged changes immediately' 
- OK
9) Right Click on '(your branchName)' Branch
- Select 'Push to'
- Just Check list PUSH and TRACK on '(your branchName)' Branch 
- OK
- If success, you'll see (your branchName) on Remotes Section
10) Done !

